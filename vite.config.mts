import { defineConfig } from 'vite';
import wgsl from "./src/wgsl";
import typescript from "@rollup/plugin-typescript";


export default defineConfig({
    plugins: [
        wgsl(),
    ],
    resolve: {
        alias: {
        }
    },
    build: {
        emptyOutDir: true,
        minify: false,
        rollupOptions: {

            plugins: [
                typescript()
            ]
        },
        lib: {
            name: "ygpu",
            formats: ["es"],
            entry: "./src/lib.ts",
            fileName: () => "ygpu.js"
        }
    },
});
