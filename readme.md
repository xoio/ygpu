ygpu
====

This is a toy WebGPU renderer intended for smaller scale experiments. 

Setup
=== 
* `npm install`
* `npm run build` to build into a library form. 

Building Samples
===
* Samples are built using the `buildSampleList.mjs` script 
* Once that is run, you will get a `samplellist.ts` file in `./src/samples`
* Launching the dev server will bring up a list of samples; click on the sample you want to see.

GLMatrix
===
[GLMatrix](https://github.com/toji/gl-matrix) is used as the main math library. This version included is the beta version so some things may not behave and 
look different than expected. Note that it is excluded in tsconfig to get rid of a couple warnings that pop up during build. 

WGSL notes
===
* Note that this uses a custom Vite plugin that will turn normal GLSL 440+ into WGSL. The plugin uses [Naga](https://github.com/gfx-rs/wgpu/tree/trunk/naga) to do the translation. See that
project for any additional information if the translation is not occurring as expected. This is mainly used for the included shaders and samples 
but a web compatible version is included in the build to use if you want.


