import fs from "fs"

let samples = fs.readdirSync("./src/samples")


samples = samples.map((itm) => {
    if(itm !== "shaders" && itm !== "samplelist.ts"){
        return {
            name:itm.replace(".ts",""),
            path: `./samples/${itm}`
        }
    }
}).filter(itm => itm !== undefined)

fs.writeFileSync("./src/samples/samplelist.ts",`export default ${JSON.stringify(samples)}`)
