import {GPUInstance, ShaderModule} from "../types";
import {CULL_BACK, DEPTH24_PLUS, DEPTH_COMPARE_LESS, TRIANGLE_LIST} from "../constants";

/**
 * Creates a Render Pipeline for rendering graphics
 * @param ctx
 * @param vertex
 * @param fragment
 * @param topology
 * @param layout
 * @param hasDepth
 * @param cullMode
 * @param depthSettings
 */
export function createGraphicsPipeline(
    ctx: GPUInstance | GPUDevice,
    vertex: ShaderModule,
    fragment: ShaderModule,
    {
        topology = TRIANGLE_LIST,
        layout = "auto",
        hasDepth = false,
        cullMode = CULL_BACK,
        depthSettings = {
            depthWriteEnabled: true,
            depthCompare: DEPTH_COMPARE_LESS,
            format: DEPTH24_PLUS,

        },
        label = "Pipeline",
    }: {
        topology?: string,
        hasDepth?: boolean,
        depthSettings?: any,
        cullMode?: string,
        label?: string
        layout?: GPUPipelineLayout | string
    } = {}
) {

    const props = {
        layout,
        vertex,
        fragment,
        label,
        primitive: {
            topology,
            cullMode
        },

    } as unknown as GPURenderPipelineDescriptor

    if (hasDepth) {
        props.depthStencil = depthSettings
    }

    return ctx.createRenderPipeline(props)
}

