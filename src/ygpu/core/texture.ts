import {GPUInstance} from "../types";
import {BGRA8UNORM, DEFAULT_TEXTURE_USAGE, RENDER_ATTACHMENT, TEX_COPY_DST} from "../constants";

/**
 * Constructs a texture based on the settings provided. Defaults to trying to create a texture for rendering.
 * @param ctx
 * @param size
 * @param texture_format
 * @param usage
 * @param image
 * @param label
 */
export function createTexture(
    ctx: GPUInstance,
    {
        size = [1, 1],
        texture_format = BGRA8UNORM as GPUTextureFormat,
        usage = DEFAULT_TEXTURE_USAGE,
        image = null,
        label="texture"
    }: {
        size?: number[],
        texture_format?: string | GPUTextureFormat,
        usage?: number,
        image?: HTMLImageElement | ImageBitmap | null,
        label?:string
    } = {}
) {

    if (image) {
        return ctx.createTextureFromImage(image as ImageBitmap, {
            size: [image.width, image.height],
            format: texture_format as GPUTextureFormat,
            usage,
            label
        })
    } else {
        const tex = ctx.createTexture({
            size,
            format: texture_format as GPUTextureFormat,
            usage,
            label
        })

        // fill with dummy data
        let len = size[0] * size[1] * 4
        let data = new Uint8Array(len)
        for (let i = 0; i < len; ++i) {
            data[i] = Math.random()
        }

        ctx.writeTexture(tex, data, {
            width: size[0],
            height: size[1]
        })

        return tex
    }
}
