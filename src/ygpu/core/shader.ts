import {GPUInstance} from "../types";

/**
 * Builds a shader structure
 * @param ctx {GPUInstance} an instance object or an object that contains the [GPUDevice] object used for your app
 * @param vertex {string} the vertex shader code. Code should already be in WGSL
 * @param fragment {string} the fragment shader code. Code should already be in WGSL
 * @param buffers {Array} an array of [GPUBuffer] objects
 * @param fragmentTargets {Array} an array of strings listing formats that the fragment stage will use
 * @param vertexEntry {string} the name of the vertex shader entry function
 * @param fragmentEntry {string} the name of the fragment shader entry function
 */
export function createShader(
    ctx: GPUInstance | GPUDevice,
    vertex: string,
    fragment: string,
    {
        buffers = [],
        fragmentTargets = [
            {
                format: navigator.gpu.getPreferredCanvasFormat()
            }
        ],
        vertexEntry = "main",
        fragmentEntry = "main"
    }: {
        buffers?: GPUVertexBufferLayout[],
        fragmentTargets?: Array<GPUColorTargetState>
        vertexEntry?: string,
        fragmentEntry?: string
    } = {}) {


    if (vertex === "" || fragment === "") {
        console.error("Error during shader creation. Either a vertex or fragment shader is missing")
        return
    }

    return {
        vertex: {
            module: ctx.createShaderModule({
                code: vertex
            }),
            entryPoint: vertexEntry,
            buffers
        },
        fragment: {
            module: ctx.createShaderModule({
                code: fragment,
            }),
            entryPoint: fragmentEntry,
            targets: fragmentTargets
        }
    }

}
