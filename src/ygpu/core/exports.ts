import {createShader} from "./shader";
import {createInstance} from "./instance";


export default {
    createShader,
    createInstance
}
