import {GPUInstance, ShaderModule} from "../types";

/**
 * Creates a Compute pipeline
 * @param ctx
 * @param shader
 * @param layout
 */
export function createComputePipeline(
    ctx: GPUInstance | GPUDevice,
    {
        shader = "",
        layout = null
    }: {
        shader?: string,
        layout?: GPUPipelineLayout | null
    } = {}
) {

    if (shader === "") {
        console.log("Attempt to build compute pipeline without a shader. ")
        return
    }

    const desc: GPUComputePipelineDescriptor = {
        layout: layout !== null ? layout : "auto",
        compute: {
            module: ctx.createShaderModule({
                code: shader
            })
        }
    }

    return ctx.createComputePipeline(desc)
}
