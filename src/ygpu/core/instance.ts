import {GPUInstance} from "../types";

/**
 * Builds the main instance object for WebGPU handling
 * @param canvas {HTMLCanvasElement} the canvas element to use for this instance
 *
 * @return {GPUInstance} returns a JS object that contains the adapter, context, device, as well as some helper
 * functions for ease of use.
 */
export async function createInstance(
    {
        canvas = null
    }: {
        swapchainFormat?: string,
        canvas?: HTMLCanvasElement | OffscreenCanvas | null
    } = {}) {

    if (!navigator.gpu) {
        alert("Your browser does not support WebGPU");
        return;
    }

    if (!canvas) {
        console.log("No canvas passed into createInstance")
        return
    }

    // ctx can be null, for example in the context of Deno
    let ctx = canvas!.getContext("webgpu");

    let adapter = await navigator.gpu.requestAdapter()
    let device = await adapter!.requestDevice()

    ctx?.configure({
        device,
        format: navigator.gpu.getPreferredCanvasFormat(),
        alphaMode: "premultiplied"
    })

    return {
        adapter,
        device,
        ctx,
        name: "GPUInstance",
        /**
         * Creates a sampler object
         * @param opts
         */
        createSampler(opts: GPUSamplerDescriptor) {
            return device.createSampler(opts)
        },

        /**
         * Creates a shader module object
         * @param opts
         */
        createShaderModule(opts: GPUShaderModuleDescriptor) {
            return device.createShaderModule(opts)
        },

        getPreferredFormat() {
            return navigator.gpu.getPreferredCanvasFormat()
        },

        getView() {
            return ctx?.getCurrentTexture().createView()
        },

        addCanvasHelper(canvas: HTMLCanvasElement, {
            resizeCb = () => {

                canvas.width = window.innerWidth
                canvas.height = window.innerHeight
            }
        } = {}) {

            canvas.width = window.innerWidth
            canvas.height = window.innerHeight

            window.addEventListener("resize", resizeCb)

        },

        /**
         * Creates a bind group layout
         * @param opts
         */
        createBindGroupLayout(opts: GPUBindGroupLayoutDescriptor) {
            return device.createBindGroupLayout(opts)
        },

        createBindGroup(desc: GPUBindGroupDescriptor) {
            return device.createBindGroup(desc)
        },

        /**
         * Creates a pipeline layout object
         * @param desc
         */
        createPipelineLayout(desc: GPUPipelineLayoutDescriptor) {
            return device.createPipelineLayout(desc)
        },

        createRenderPipeline(desc: GPURenderPipelineDescriptor) {

            return device.createRenderPipeline(desc)
        },

        /**
         * Helper to create data buffers. Will automatically set initial data if
         * "mappedAtCreation" is true, and you pass in data
         * @param desc {GPUBufferDescriptor}
         * @param data {Float32Array|Uint16Array} a typed array with the data you want to fill
         */
        createBuffer(desc: GPUBufferDescriptor, data: Float32Array | Uint16Array | Uint32Array | null = null) {
            let buffer = device.createBuffer(desc);

            if (desc.mappedAtCreation && data !== null) {

                if (data instanceof Float32Array) {
                    new Float32Array(buffer.getMappedRange()).set(data)
                }

                if (data instanceof Uint16Array) {
                    new Uint16Array(buffer.getMappedRange()).set(data)
                }

                if (data instanceof Uint32Array) {
                    new Uint32Array(buffer.getMappedRange()).set(data)
                }

                buffer.unmap()
            }

            // make sure to unmap if buffer mapped by mistake
            if (desc.mappedAtCreation && data === null) {
                buffer.unmap()
            }

            return buffer
        },

        createComputePipeline(desc: GPUComputePipelineDescriptor) {
            return device.createComputePipeline(desc)
        },

        createTextureFromImage(bitmap: ImageBitmap, opts: GPUTextureDescriptor) {
            let tex = device.createTexture(opts)
            device.queue.copyExternalImageToTexture({
                source: bitmap
            }, {
                texture: tex
            }, [
                bitmap.width, bitmap.height
            ])

            return tex
        },

        createTexture(opts: GPUTextureDescriptor) {
            return device.createTexture(opts)
        },

        submit(subs: Array<GPUCommandBuffer>) {
            device.queue.submit(subs)
        },

        createCommandEncoder() {
            return device.createCommandEncoder()
        },

        /**
         * Writes data to the specified buffer
         * @param buffer {GPUBuffer}
         * @param bufferOffset {number}
         * @param data {BufferSource}
         * @param dataOffset {number}
         * @param size {number}
         */
        writeBuffer(buffer: GPUBuffer, bufferOffset: number, data: BufferSource, dataOffset: number = 0, size: number = 0) {
            device.queue.writeBuffer(
                buffer,
                bufferOffset,
                data,
                dataOffset,
                size
            )
        },

        writeTexture(destination: GPUTexture, data: ArrayBufferView, {
            width = 1,
            height = 1,
            bytesPerRow = width * 4,
            offset = 0,
            rowsPerImage = 0
        } = {}) {


            device.queue.writeTexture(
                {texture: destination},
                data,
                {
                    offset,
                    bytesPerRow
                },
                {
                    width,
                    height
                }
            )
        }
    } as GPUInstance
}
