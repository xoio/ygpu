import {createBindGroup} from "./bindgroup";
import {createBindGroupEntries} from "./bindgroup_entry";
import {createBindGroupLayout} from "./bindgroup_layout";

export default   {
    createBindGroup,
    createBindGroupEntries,
    createBindGroupLayout
}
