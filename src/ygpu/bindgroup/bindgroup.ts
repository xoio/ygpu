import {GPUInstance} from "../types";
import {createBindGroupLayout} from "./bindgroup_layout";

/**
 * Makes a BindGroup (aka descriptor set if you're coming from Vulkan world)
 * @param ctx
 * @param entries
 * @param layout_entries
 * @param layout
 * @param label
 */
export function createBindGroup(
    ctx: GPUInstance | GPUDevice,
    entries: Array<GPUBindGroupEntry>,
    {
        layout_entries = [],
        layout = null,
        label = "bindgroup",
        layout_label = "createBindgroup layout label"
    }: {
        layout_entries?: Array<GPUBindGroupLayoutEntry>,
        layout?: GPUBindGroupLayout | null,
        label?: string,
        layout_label?: string
    } = {}
) {

    // build layout if it is not defined but we've specified layout entries.
    let bg_layout = null
    if (layout === null && layout_entries.length > 0) {

        bg_layout = createBindGroupLayout(ctx, layout_entries, {
            label: layout_label
        })
    }

    if (!bg_layout) {

        // assuming layout_entries is empty, finish determining whether or not to use the "auto" qualifier
        // for the bindgroup layout
        if (layout === null && layout_entries.length === 0) {
            bg_layout = "auto"
        } else if (layout) {
            bg_layout = layout
        }
    }

    const struct = {
        entries,
        layout: bg_layout,
        label
    } as GPUBindGroupDescriptor

    return ctx.createBindGroup(struct)
}
