export type BindGroupEntry = {
    location: number,
    shader: number,

    // since more often than not we just need type, buffer just requires the uniform type
    buffer?: string
    bufferHasDynamicOffset?: boolean
    bufferMinBindSize?: number

    sampler?: any
    sampler_type?: GPUSamplerBindingType

    texture?: any
    multisampled_texture?: boolean
    texture_sample_type?: string
    texture_view_dimension?: string
    label?: string
}

/**
 * Provides a more simplified approach to adding bind group entries while also attempting to
 * rename some things so that they make more sense, ie, using the word "shader" instead of "visibility"
 * to denote which shaders the bind group entry should be associated with.
 *
 * Bindgroup entries is used to write the metadata for resources you plan to use. Make sure to denote each entry's
 * type by specifying either "sampler", "texture" or "buffer". If you aren't setting any other properties, you can
 * leave the objects empty.
 *
 * @param entries
 */
export function createBindGroupEntries(entries: Array<BindGroupEntry>) {

    return entries.map(itm => {
        let entry: any = {
            binding: itm.location,
            visibility: itm.shader,
        }

        if (itm.buffer !== undefined) {
            entry["buffer"] = {
                type: itm.buffer,
                hasDynamicOffset: itm.bufferHasDynamicOffset !== undefined ? itm.bufferHasDynamicOffset : false,
                minBindingSize: itm.bufferMinBindSize !== undefined ? itm.bufferMinBindSize : 0
            }
        }

        if (itm.sampler !== undefined) {
            entry["sampler"] = {
                type: itm.sampler_type !== undefined ? itm.sampler_type : "filtering",
            }
        }

        if (itm.texture !== undefined) {
            entry["texture"] = {
                multisampled: itm.multisampled_texture !== undefined ? itm.multisampled_texture : false,
                sampleType: itm.texture_sample_type !== undefined ? itm.texture_sample_type : "float",
                viewDimension: itm.texture_view_dimension !== undefined ? itm.texture_view_dimension : "2d"
            }
        }

        return entry
    })
}
