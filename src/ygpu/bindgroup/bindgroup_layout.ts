import {GPUInstance} from "../types";


export function createBindGroupLayout(
    ctx: GPUInstance | GPUDevice,
    entries: Array<GPUBindGroupLayoutEntry>,
    {
        label = "unnamed bind group layout"
    }={}
) {

    // build layout
    return ctx.createBindGroupLayout({
        entries,
        label
    })
}

