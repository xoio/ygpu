/**
 * Defines the core component that contains the key elements for working with WebGPU
 * Note that most of these could simply be called directly on the [GPUDevice] object, but having a wrapper is helpful at times, for example
 * doing some additional setup when making textures or filling buffers.
 */
export type GPUInstance = {
    adapter: GPUAdapter,
    device: GPUDevice,
    ctx: GPUCanvasContext,
    name: string,
    createSampler: (opts:GPUSamplerDescriptor) => GPUSampler
    createShaderModule:(opts:GPUShaderModuleDescriptor) => GPUShaderModule
    createBindGroupLayout: (opts:GPUBindGroupLayoutDescriptor) => GPUBindGroupLayout
    createPipelineLayout: (opts:GPUPipelineLayoutDescriptor) => GPUPipelineLayout
    createBuffer: (desc:GPUBufferDescriptor, data:Float32Array | Uint16Array| Uint32Array) => GPUBuffer
    createBindGroup: (opts:GPUBindGroupDescriptor) => GPUBindGroup
    createTextureFromImage: (bitmap:ImageBitmap,opts:GPUTextureDescriptor) => GPUTexture
    createTexture: (opts:GPUTextureDescriptor) => GPUTexture
    submit: (subs:Array<GPUCommandBuffer>) => void
    createCommandEncoder: Function
    createRenderPipeline: (desc:GPURenderPipelineDescriptor) => GPURenderPipeline,
    writeBuffer: (buffer:GPUBuffer, bufferOffset:number, data:BufferSource, dataOffset?:number = 0,size:?number = 0) => void
    writeTexture: (destination: GPUTexture, data: ArrayBufferView, {
        width = 1,
        height = 1,
        bytesPerRow = width * 4,
        offset = 0,
        rowsPerImage = 0
    } = {}) => void

    getPreferredFormat: Function
    getView: Function
    addCanvasHelper: (canvas:HTMLCanvasElement) => void
    createComputePipeline: (desc:GPUComputePipelineDescriptor) => GPUComputePipeline
}

/**
 * Defines a shader module.
 * Depending on the type, you pass in an array of GPUBuffers for content when
 * defining a vertex shader or an array of strings with defining a fragment shader.
 */
export type ShaderModule = {
    module: GPUShaderModule,
    entryPoint: string,
    buffers?: Array<GPUVertexBufferLayout>
    targets?: Array<GPUColorTargetState>
}
