// a listing of shorthands for various constant type references in JS for WebGPU

// default function to look for each shader module.
export const DEFAULT_SHADER_ENTRY = "main"

/// Numerical types.
export const FLOAT2: string = "float2";
export const FLOAT3: string = "float3";
export const FLOAT4: string = "float4";
export const MAT4: string = "mat4";
export const FLOAT: string = "float";

export const FLOAT32_4 = "float32x4";
export const FLOAT32_3 = "float32x3";
export const FLOAT32_2 = "float32x2";
export const FLOAT32 = "float32";

//////////// FILTERING TYPES ///////////////
export const PREMULTIPLIED = "premultiplied"
export const CLAMP_TO_EDGE: string = "clamp-to-edge"
export const LINEAR_FILTER:string = "linear"
export const NON_FILTERING:string = "non-filtering"

////// SHADER TYPES ////////////
export const SHADER_VERTEX = GPUShaderStage.VERTEX;
export const SHADER_FRAGMENT = GPUShaderStage.FRAGMENT;
export const SHADER_COMPUTE = GPUShaderStage.COMPUTE;

// helper to indicate something should be used in both the vertex and fragment stages.
export const SHADER_RASTER = GPUShaderStage.VERTEX | GPUShaderStage.FRAGMENT

/////// OPERATIONS //////////////////
// TODO note that for these, typing to string seems to cause lint issues, so just letting inference take over
export const CLEAR_OP = "clear"
export const STORE_OP  = "store";
export const DISCARD_OP = "discard"

/////// TOPOLOGY TYPES ///////////////////
export const TRIANGLE_LIST: string = "triangle-list"
export const POINT_LIST: string = "point-list"
export const LINE_LIST: string = "line-list"
export const TRIANGLE_STRIP = "triangle-strip"

/////// TEXTURE FORMATS ////////////////
export const DEPTH24_STENCIL8 = "depth24plus-stencil8"
export const DEPTH24_PLUS = "depth24plus";
export const DEPTH_COMPARE_LESS = "less"
export const BGRA8UNORM: string = "bgra8unorm";
export const RGBA8UNORM: string = "rgba8unorm"

export const RGBA32FLOAT: string = "rgba32float"
export const RGBA16FLOAT: string = "rgba16float"

export const UNFILTERABLE_FLOAT:string = "unfilterable-float"

export const LINEAR: string = "linear";
export const VERTEX_STEP_MODE = "vertex";
export const INSTANCE_STEP_MODE = "instance";

export const INDEX_USAGE = GPUBufferUsage.INDEX
export const VERTEX_USAGE = GPUBufferUsage.VERTEX

export const INDIRECT_USAGE = GPUBufferUsage.INDIRECT

/// Texture specific
export const DEFAULT_TEXTURE_USAGE = GPUTextureUsage.RENDER_ATTACHMENT | GPUTextureUsage.TEXTURE_BINDING | GPUTextureUsage.COPY_DST;

export const RENDER_ATTACHMENT = GPUTextureUsage.RENDER_ATTACHMENT;
export const TEX_COPY_DST = GPUTextureUsage.COPY_DST

export const SAMPLER: string = "sampler";
//export const SAMPLER_TEXTURE: string = "sampled-color"


// general type arrays
export const FLOAT32_ARRAY = "Float32Array"
export const UINT16 = "uint16"
export const UINT32 = "uint32"

/// Buffer usage specific
export const UNIFORM_USAGE = GPUBufferUsage.UNIFORM;
export const STORAGE_USAGE = GPUBufferUsage.STORAGE;

export const COPY_DST_USAGE = GPUBufferUsage.COPY_DST


export const UNIFORM_TYPE = "uniform";
export const STORAGE_TYPE = "storage";
export const BUFFER_TYPE = "buffer"
export const STORAGE_BUFFER: string = 'storage-buffer';
export const UNIFORM_BUFFER: string = "uniform-buffer"

/// sample types
export const SAMPLE_FLOAT = "float"
export const SAMPLE_FILTER = "filtering"

/// cull modes
export const CULL_BACK: string = "back";
export const CULL_FRONT: string = "front"

/// math related
export const PI = 3.141592653589793238462643383279502884197169399375105820974944;
export const PI_2 = PI * PI;

// color state related
