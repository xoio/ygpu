/**
 * Returns some general settings for doing alpha blending.
 * Note that of course these make some assumptions but should be fine
 * for most situations
 */
export function getAlphaBlendingSettings(): GPUBlendState {
    return {
        color: {
            operation: "add",
            srcFactor: 'src-alpha',
            dstFactor: 'one-minus-src-alpha'
        },
        alpha: {
            operation: "add",
            srcFactor: "one",
            dstFactor: "one-minus-src-alpha"
        },
    }
}
