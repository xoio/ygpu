import FrameBuffer from "./framebuffer";
import {PerspectiveCamera} from "./camera";
import OrbitCamera from "./orbitcamera";

export default {
    FrameBuffer,
    OrbitCamera,
    PerspectiveCamera
}
