import {GPUInstance} from "../types";
import {createTexture} from "../core/texture";
import {CLEAR_OP, STORE_OP} from "../constants";


class FrameBuffer {

    ctx: GPUInstance
    attachments: Array<GPUTexture> = []
    size: [number, number]
    clearValue: [number, number, number, number]

    encoder: GPURenderPassEncoder | null = null
    loadOp: GPULoadOp = CLEAR_OP
    storeOp: GPUStoreOp = STORE_OP

    constructor(
        ctx: GPUInstance,
        width: number,
        height: number
    ) {
        this.ctx = ctx
        this.size = [width, height]
        this.clearValue = [0, 0, 0, 1]

        // add base layer
        this.addAttachment()

    }

    addAttachment(size: number[] = []) {
        this.attachments.push(createTexture(this.ctx, {
            size: size.length > 0 ? size : this.size
        }))
    }

    /**
     * Returns the specified attachment
     * @param idx
     */
    getAttachment(idx: number = 0) {
        return this.attachments[idx]
    }

    /**
     * Returns the specified attachment's [GPUTextureView]
     * @param idx
     */
    getAttachmentView(idx: number) {
        return this.getAttachment(idx).createView()
    }

    generateRenderPassDescriptor(idx: number = 0): GPURenderPassDescriptor {
        return {
            colorAttachments: [
                {
                    view: this.getAttachmentView(idx),
                    clearValue: this.clearValue,
                    loadOp: this.loadOp,
                    storeOp: this.storeOp
                }
            ]
        }
    }

    /**
     * Starts the render
     * @param cb
     */
    bind(cb: GPUCommandEncoder) {
        this.encoder = cb.beginRenderPass(this.generateRenderPassDescriptor())
        return this.encoder
    }

    /**
     * Unbinds the framebuffer
     */
    unbind() {
        if (this.encoder) {
            this.encoder.end()
            this.encoder = null
        }
    }
}

export default FrameBuffer
