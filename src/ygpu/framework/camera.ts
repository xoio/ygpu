import {mat4, Mat4Like} from "../../math/glmatrix/mat4";

export function deg2Rad() {
    return 0.017453292519943295
}

export class PerspectiveCamera {
    aspect: number
    center: Float32Array = new Float32Array([0, 0, 0])
    eye: Float32Array = new Float32Array([0, 0, 0])
    far: number
    fov: number
    near: number;
    position: Float32Array = new Float32Array([0, 0, 0])
    projectionMatrix: Float32Array | Mat4Like = mat4.create()
    target: Float32Array = new Float32Array([0, 0, 0])
    up: Float32Array = new Float32Array([0, 0, 0])
    viewMatrix: Float32Array | Mat4Like = mat4.create()

    matrixWorld: Float32Array = mat4.create()

    frustumTop: any;
    frustumBottom: any;
    frustumRight: any;
    frustumLeft: any;

    cameraData:Float32Array= mat4.create()

    constructor(fov: number, aspect: number, near: number = 0.1, far = 1000.0) {
        this.up[1] = 1;
        mat4.identity(this.viewMatrix);
        mat4.identity(this.projectionMatrix);
        mat4.identity(this.matrixWorld)

        this.fov = fov
        this.aspect = aspect
        this.near = near
        this.far = far

        this.setZoom(1);
        this.perspective()
    }
    resize(aspect:number=window.innerWidth/window.innerHeight){
        this.aspect = aspect
        this.setZoom(1);
        this.perspective()

    }
    getViewProjectionMatrix(){
        // @ts-ignore
        this.cameraData.set(mat4.multiply(mat4.create(),this.projectionMatrix,this.viewMatrix));
        return this.cameraData;
    }

    getViewMatrix(){
        return this.viewMatrix
    }

    getProjectionMatrix(){
        return this.projectionMatrix
    }

    /**
     * Scales FOV to match the canvas so that values approximately match normal DOM values.
     * @param canvasHeight
     */
    scaleFovToCanvas(canvasHeight: number) {
        this.fov = 2 * Math.atan(canvasHeight / (2 * this.eye[2])) * (180 / Math.PI);

        // hold onto current zoom
        let tmp = this.eye[2]

        // need to temporarily reset to get an accurate projection matrix
        this.setZoom(1)

        this.perspective();

        // reset to current zoom
        this.setZoom(tmp)
    }

    /**
     * Adapted from Three.js projection matrix calculation combining
     * https://github.com/mrdoob/three.js/blob/3dabc5a09f9b312d3d713cddb5f0c989b4ba6b4e/src/cameras/PerspectiveCamera.js#L179
     * https://github.com/mrdoob/three.js/blob/3dabc5a09f9b312d3d713cddb5f0c989b4ba6b4e/src/math/Matrix4.js#L777
     */
    perspective() {
        const near = this.near
        const far = this.far
        const aspect = this.aspect

        let top = near * Math.tan(deg2Rad() * 0.5 * this.fov) / this.eye[2];
        let height = 2 * top;
        let width = aspect * height;
        let left = -0.5 * width;
        let right = left + width
        let bottom = top - height

        const x = 2 * this.near / (right - left);
        const y = 2 * this.near / (top - bottom);
        const a = (right + left) / (right - left);
        const b = (top + bottom) / (top - bottom);
        let c = -(far + near) / (far - near);
        let d = (-2 * far * near) / (far - near);

        let te = this.projectionMatrix
        te[0] = x;
        te[4] = 0;
        te[8] = a;
        te[12] = 0;
        te[1] = 0;
        te[5] = y;
        te[9] = b;
        te[13] = 0;
        te[2] = 0;
        te[6] = 0;
        te[10] = c;
        te[14] = d;
        te[3] = 0;
        te[7] = 0;
        te[11] = -1;
        te[15] = 0;
    }


    /**
     * Updates the projection matrix
     * @param aspect {number} the new aspect ratio
     * @param fov {number} the new fov value - if nothing is passed in, we use the current fov value
     * @param near {number} the new near value - if nothing is passed in we use the current near value
     * @param far {number} the new far value - if nothing is passed in we use the current far value.
     */
    updateProjection(aspect: number, {
        fov = this.fov,
        near = this.near,
        far = this.far
    }: {
        fov?: number
        near?: number
        far?: number
    } = {}) {

        // hold onto current zoom
        let tmp = this.eye[2]

        // update new values as needed.
        this.fov = fov;
        this.aspect = aspect;
        this.near = near;
        this.far = far;

        // need to temporarily reset to get an accurate projection matrix
        this.setZoom(1)

        this.perspective();

        // reset to current zoom
        this.setZoom(tmp)

    }

    /**
     * Returns inverted matrix of projectionMatrix * view matrix
     * Useful for Raycasting
     */
    getInverseProjectionView() {
        let inverse = mat4.create();
        mat4.multiply(inverse, this.projectionMatrix, this.viewMatrix);
        mat4.invert(inverse, inverse);
        return inverse;
    }

    /**
     * Gets the inverse projection matrix.
     */
    getInverseProjection() {
        let inverse = mat4.create();
        mat4.invert(inverse, this.projectionMatrix);
        return inverse;
    }

    /**
     * Gets the inverse view matrix.
     */
    getInverseView() {
        let inverse = mat4.create();
        mat4.invert(inverse, this.viewMatrix);
        return inverse;
    }

    /**
     * Transilates the camera
     * @param position {Array<number>} a 3 item array indicating t6he x,y,z position to translate the camera.
     */
    translate(position: [number, number, number]) {
        this.eye[0] = position[0];
        this.eye[1] = position[1];

        // so we can maintain z position, z position will stay the same if position parameter z position is 0
        this.eye[2] = position[2] !== 0 ? position[2] : this.eye[2];
        mat4.identity(this.viewMatrix);
        mat4.lookAt(this.viewMatrix, this.eye, this.center, this.up)
    }

    /**
     * sets the look-at position of the view matrix based on current settings.
     */
    cameraLookAt() {
        mat4.lookAt(this.viewMatrix, this.eye, this.center, this.up);
    }

    /**
     * Calculates the camera frustrum
     */
    calcFrustum() {
        this.frustumTop = this.near * Math.tan(3.14149 / 180.0 * this.fov * 0.5);
        this.frustumBottom = -this.frustumTop;
        this.frustumRight = this.frustumTop * this.aspect
        this.frustumLeft = -this.frustumRight;
    }

    /**
     * Creates a projection matrix for the purposes of picking objects in an
     * FBO
     * https://webgl2fundamentals.org/webgl/lessons/webgl-picking.html
     * @param pixelX
     * @param pixelY
     * @param gl
     */
    createPickingProjection(pixelX: number, pixelY: number, gl: WebGL2RenderingContext) {
        this.calcFrustum();
        const aspect = this.aspect;
        const top = Math.tan(this.fov * 0.5) * this.near;
        const bottom = -top;
        const left = aspect * bottom;
        const right = aspect * top;
        const width = Math.abs(right - left);
        const height = Math.abs(top - bottom);

        const subLeft = left + pixelX * width / gl.canvas.width;
        const subBottom = bottom + pixelY * height / gl.canvas.height;
        const subWidth = 1 / gl.canvas.width;
        const subHeight = 1 / gl.canvas.height;

        return mat4.frustum(mat4.create(), subLeft, subLeft + subWidth, subBottom, subBottom + subHeight, this.near, this.far)
    }

    setViewMatrix(mat:Mat4Like){
        if(mat.length > 0){
            mat4.copy(this.viewMatrix,mat);
        }
    }

    /**
     * Calculates the ray for the camera
     * TODO need to finish
     * @param uPos
     * @param vPos
     * @param imagePlaneAspectRatio
     */
    calcRay(uPos: number, vPos: number, imagePlaneAspectRatio: number) {
        let s = (uPos - 0.5 + 0.5) * imagePlaneAspectRatio;
        let t = (vPos - 0.5 + 0.5);
        let viewDistance = imagePlaneAspectRatio / Math.abs(this.frustumRight - this.frustumLeft) * this.near;
    }

    /**
     * Sets the z position of the eye
     * @param val
     */
    setZoom(val: number, targetTo = false) {

        this.eye[2] = val;
        mat4.identity(this.viewMatrix);

        if (targetTo) {
            mat4.targetTo(this.viewMatrix, this.eye, this.center, this.up)
        } else {
            mat4.lookAt(this.viewMatrix, this.eye, this.center, this.up)
        }
    }
}
