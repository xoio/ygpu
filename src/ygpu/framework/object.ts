import {UINT32} from "../constants";

/**
 * An abstraction object you want to render / run
 */
export type RndrObject = {
    pipeline: GPURenderPipeline | GPUComputePipeline
    bindgroups: Array<GPUBindGroup>,
    vertexBuffers?: Array<GPUBuffer>,
    vertexCount?: number
    numInstances?: number
    isIndexed?: boolean,
    firstVertex?: number
    firstInstance?: number
    indexCount?: number,
    baseVertex?: number

    indexBuffer?: GPUBuffer,
    // for compute pipelines - represents this dispatch workgroup counts
    // note that in WebGPU the limit is usually 256 per dimension
    compute?: {
        xCount: number,
        yCount: number
        zCount: number
    }
}

/**
 * Renders a object to the scene.
 * @param obj {RndrObject} a RndrObject containing all the necessary components like pipeline, bindgroups, etc
 * @param passEncoder {GPURenderPassEncoder} a renderpass encoder
 */
export function renderObject(obj: RndrObject, passEncoder: GPURenderPassEncoder) {
    passEncoder.setPipeline(obj.pipeline as GPURenderPipeline)

    obj.bindgroups.forEach((itm, idx) => {
        passEncoder.setBindGroup(idx, itm)
    })

    // bind vertex buffers - remember location is determined by array order.
    obj.vertexBuffers?.forEach((itm, idx) => {
        passEncoder.setVertexBuffer(idx, itm)
    })


    if (obj.indexCount !== undefined && obj.indexBuffer !== undefined && obj.indexBuffer !== null) {
        const indexCount = obj.indexCount
        const firstVertex = obj.firstVertex !== undefined ? obj.firstVertex : 0
        const firstInstance = obj.firstInstance !== undefined ? obj.firstInstance : 0
        const numInstances = obj.numInstances !== undefined ? obj.numInstances : 1
        const baseVertex = obj.baseVertex !== undefined ? obj.baseVertex : 0

        passEncoder.setIndexBuffer(obj.indexBuffer, UINT32)
        passEncoder.drawIndexed(indexCount, numInstances, firstVertex, baseVertex, firstInstance)

    }

    if (obj.vertexCount !== undefined) {

        const numInstances = obj.numInstances !== undefined ? obj.numInstances : 1
        const firstVertex = obj.firstVertex !== undefined ? obj.firstVertex : 0
        const firstInstance = obj.firstInstance !== undefined ? obj.firstInstance : 0
        passEncoder.draw(obj.vertexCount, numInstances, firstVertex, firstInstance)
    }

}

/**
 * Runs the compute pipeline
 * @param obj
 * @param passEncoder
 * @param xCount
 * @param yCount
 * @param zCount
 */
export function runCompute(
    obj: RndrObject,
    passEncoder: GPUComputePassEncoder
) {
    passEncoder.setPipeline(obj.pipeline as GPUComputePipeline)

    obj.bindgroups.forEach((itm, idx) => {
        passEncoder.setBindGroup(idx, itm)
    })

    let xCount = obj.compute?.xCount as number
    let yCount = obj.compute?.yCount as number
    let zCount = obj.compute?.zCount as number

    // make sure all values are at least 1
    if(xCount === 0){
        xCount = 1
    }
    if(yCount === 0){
        yCount = 1
    }
    if(zCount === 0){
        zCount = 1
    }
    passEncoder.dispatchWorkgroups(xCount, yCount, zCount)
}
