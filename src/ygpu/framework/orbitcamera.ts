/**
 * Based(well, essentially ported) on the OrbitCamera class written by @Toji
 * https://github.com/toji/webgpu-metaballs/blob/main/js/camera.js#L180
 */
import {vec3} from "../../math/glmatrix/vec3";
import {mat4} from "../../math/glmatrix/mat4";


class OrbitCamera {
    projection: Float32Array = mat4.create();
    view: Float32Array = mat4.create();
    distance: Float32Array = vec3.create();

    cameraTransforms: Float32Array = mat4.create();

    position: Float32Array = vec3.create();

    orbitX: number = Math.PI * 0.1;
    orbitY: number = 0;
    maxOrbitX: number = Math.PI * 0.5;
    minOrbitX: number = -Math.PI * 0.5;
    maxOrbitY: number = Math.PI;
    minOrbitY: number = -Math.PI;
    constrainXOrbit: boolean = true;
    constrainYOrbit: boolean = false;

    maxDistance: number = 1400;
    minDistance: number = 1;

    distanceStep: number = 0.005;
    constrainDistance: boolean = true;

    dirty: boolean = true;
    moving: boolean = false;
    lastX: number = 0;
    lastY: number = 0;

    target: Float32Array = new Float32Array(3)

    constructor(
        {
            zoom = 10
        } = {}
    ) {
        this._setupListeners()
        this.distance[2] = zoom
    }

    _setupListeners() {

        // TODO integrate touch
        // Note - pointer events required as mouse events don't work as well.
        window.addEventListener("pointerdown", this._mouseDown.bind(this));
        window.addEventListener("pointermove", this._mouseMove.bind(this));
        window.addEventListener("pointerup", this._mouseUp.bind(this));
        // @ts-ignore
        window.addEventListener("mousewheel", this._mouseWheel.bind(this), {
            passive: false
        });


    }

    /**
     * Triggered by mousedown event
     * @param e {Object} mousedown event object
     */
    _mouseDown(e: PointerEvent) {
        if (e.isPrimary) {
            this.moving = true;
        }
        this.lastX = e.pageX;
        this.lastY = e.pageY;
    }

    _mouseWheel(e: WheelEvent) {
        // @ts-ignore
        let deltaY = e.wheelDeltaY !== undefined ? e.wheelDeltaY : e.deltaY
        let val = this.distance[2] + (-deltaY * this.distanceStep);
        this.setDistance(val);
        e.preventDefault();
    }


    _mouseUp(e: PointerEvent) {
        if (e.isPrimary) {
            this.moving = false;
        }
    }

    orbit(x: number, y: number) {

        if (x || y) {
            this.orbitY += x;

            if (this.constrainYOrbit) {
                this.orbitY = Math.min(Math.max(this.orbitY, this.minOrbitY), this.maxOrbitY);
            }
        } else {
            while (this.orbitY < -Math.PI) {
                this.orbitY += Math.PI * 2;
            }
            while (this.orbitY >= Math.PI) {
                this.orbitY -= Math.PI * 2;
            }
        }

        this.orbitX += y;
        if (this.constrainXOrbit) {
            this.orbitX = Math.min(Math.max(this.orbitX, this.minOrbitX), this.maxOrbitX);
        } else {
            while (this.orbitX < -Math.PI) {
                this.orbitX += Math.PI * 2;
            }
            while (this.orbitX >= Math.PI) {
                this.orbitX -= Math.PI * 2;
            }
        }

        this.dirty = true;
    }

    _mouseMove(e: PointerEvent) {
        let xDelta, yDelta;

        //@ts-ignore
        if (document.pointerLockEnabled) {
            xDelta = e.movementX;
            yDelta = e.movementY;
            this.orbit(xDelta * 0.025, yDelta * 0.025);
        } else if (this.moving) {
            xDelta = e.pageX - this.lastX;
            yDelta = e.pageY - this.lastY;
            this.lastX = e.pageX;
            this.lastY = e.pageY;
            this.orbit(xDelta * 0.025, yDelta * 0.025);
        }

    }

    setDistance(value: number) {
        this.distance[2] = value;
        if (this.constrainDistance) {
            this.distance[2] = Math.min(Math.max(this.distance[2], this.minDistance), this.maxDistance);
        }

        this.dirty = true;
    }

    getPosition() {
        this._updateMatrices();
        vec3.set(this.position, 0, 0, 0);
        vec3.transformMat4(this.position, this.position, this.cameraTransforms);
        return this.position;
    }

    getViewMatrix() {
        this._updateMatrices();
        return this.view;
    }

    _updateMatrices() {
        if (this.dirty) {
            let mv = this.cameraTransforms;
            mat4.identity(mv);

            mat4.translate(mv, mv, this.target);
            mat4.rotateY(mv, mv, -this.orbitY);
            mat4.rotateX(mv, mv, -this.orbitX);
            mat4.translate(mv, mv, this.distance);
            mat4.invert(this.view, this.cameraTransforms);

            this.dirty = false;


        }
    }


}

export default OrbitCamera
