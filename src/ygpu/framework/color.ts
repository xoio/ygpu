import {clamp, mix} from "../../math/utils";

export function hex_to_rgb(hex: string) {
    hex = hex.replace(/^#/, '')

    if (hex.length === 3) {
        hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2]
    }

    const bigint = parseInt(hex, 16)
    const r = (bigint >> 16) & 255
    const g = (bigint >> 8) & 255
    const b = (bigint & 255)

    return [r, g, b]
}

// adapted from https://github.com/spite/codevember-2021/blob/main/modules/gradient-linear.js
export class GradientLinear {

    colors: number[][] = []

    constructor(color_set: Array<string>) {
        this.colors = color_set.map(val => {
            const col = hex_to_rgb(val)
            return [col[0], col[1], col[2], 1.]
        })
    }

    get_at(val: number) {

        let t = clamp(val, 0.0, 1.0)
        let len = this.colors.length

        let from = Math.floor((t * len * 0.9999))
        let to = clamp((from + 1.0), 0.0, 1.0)

        let fc = this.colors[from]
        let ft = this.colors[to]

        let p = (t - from / len) / (1.0 / len)
        let r = mix(fc[0], ft[0], p);
        let g = mix(fc[1], ft[1], p);
        let b = mix(fc[2], ft[2], p);

        return [r, g, b, 1.0]
    }
}
