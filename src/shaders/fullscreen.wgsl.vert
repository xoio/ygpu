#version 450
layout(location = 0) out vec2 vUv;

void main()
{
    vec2 scale = vec2(0.5);
    vec2 uv = vec2((gl_VertexIndex << 1) & 2, gl_VertexIndex & 2);
    vUv = vec2(uv.x, 1. - uv.y);
    gl_Position = vec4(uv * 2.0f + -1.0f, 0.0f, 1.0f);
}
