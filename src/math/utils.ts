/**
 * Returns a random float value between two numbers
 * @param min {Number} the minimum value
 * @param max {Number} the maximum value
 * @returns {number}
 */
export function randFloat(min: number = 0, max: number = 1) {
    //return min + Math.random() * (max - min + 1);
    return min + Math.random() * (max - min);
}

export function randomPtInSphere() {
    let lambda = Math.random()
    let u = (Math.random() * 2) - 1
    let phi = (Math.random() * 2) * Math.PI
    let x = Math.pow(lambda, (1 / 3)) * Math.sqrt(1 - (u * u)) * Math.cos(phi)
    let y = Math.pow(lambda, (1 / 2)) * Math.sqrt(1 - (u * u)) * Math.sin(phi)
    let z = Math.pow(lambda,1 / 3) * u

    return {
        x,
        y,
        z
    }
}

/**
 * Ensures a value lies in between a min and a max
 * @param value
 * @param min
 * @param max
 * @returns {*}
 */
export function clamp(value: number, min: number, max: number) {
    return min < max
        ? (value < min ? min : value > max ? max : value)
        : (value < max ? max : value > min ? min : value)
}

export function mix(x:number, y:number, a:number){
    if (a <= 0.0) {
        return x;
    }

    if (a >= 1.0) {
        return y;
    }
    return x + a * (y - x)
}
