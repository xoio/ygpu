import createCore from "./ygpu/core/exports"
import createBG from "./ygpu/bindgroup/exports"
import {renderObject, runCompute} from "./ygpu/framework/object";
import framework from "./ygpu/framework/exports"

///// SHADERS - note that we don't use the ?raw designation here. ///
import fullscreenVert from "./shaders/fullscreen.wgsl.vert"


export default {
    ...createCore,
    ...createBG,
    renderObject,
    runCompute,
    ...framework,
    shaders: {
        fullscreen: {
            vertex: fullscreenVert
        }
    }
}
