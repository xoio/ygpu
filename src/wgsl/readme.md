Juicer
===
A simple shader concatenation helper in the form of a Vite plugin.
It will also attempt to convert GLSL to WGSL using [Naga](https://github.com/gfx-rs/wgpu/tree/trunk/naga) 

To use
===
* You can use an `#include` statement in your shader and the plugin will automatically resolve and read the file, replacing the 
`#include` statement with the file contents. 
* By default, the plugin expects your shader folder to be at `src/shaders` but you can change that behavior by passing in `shader_root` as a plugin parameter
with wherever your shader folder is.

Building
===
Vite does not have a great in-built way of loading WASM binaries in the context of a plugin(or maybe it does and I just haven't found it) so building this
is a bit weird. 

* Building the plugin yourself will require Rust. 
* Clone the repo with submodules, `git clone --recurse-submodules` to download the Rust portion of the plugin
* Next, run `build_library.mjs`; this will compile the WASM portion of the plugin and output the result to the `dist` folder.
* The contents of the `dist` folder, combined with the index.ts file, forms the plugin.

WGSL
===
WGSL conversion is facilitated by Naga which is part of the [wgpu](https://github.com/gfx-rs/wgpu) library. Conversion is handled via a WebAssembly module.

* For WGSL conversion, your files must include "wgsl" in it's filename like so. 
  * `.wgsl.vert`
  * `.wgsl.frag`
* This is done to help ensure the library can distinguish between vertex and fragment shaders
* Note that compute shaders have not been tested yet.
* If you run into trouble, please check your shader against the [Naga Cli](https://github.com/gfx-rs/wgpu/tree/trunk/naga) first before opening an issue.


GLSL to WGSL Notes
===
* Naga has a requirement of your glsl code being version 440+ compliant
* This does mean you will likely have to still make changes. 
* That said changes should not be too bad, especially if you're already using WebGL2 as it should (hopefully) only be localized to how you declare your attribs, uniforms and varyings. 

Attribs Example
==
WebGL 
```glsl
  attribute vec3 position 
```

WebGL 2
```glsl
  in vec3 position 
```

WGLSL Compliant 
```glsl
  layout(location = 0) in vec3 position;
```
Varying Example
==
WebGL
```glsl
  varying vec3 position 
```

WebGL 2
```glsl
  out vec3 position 
```

WGLSL Compliant
```glsl
  layout(location = 0) in vec3 position;
```


Uniform Example
==
Uniforms are where things could possibly get a little troublesome.

WebGL/WebGL2
```
  uniform mat4 projectionMatrix
```

WGLSL Compliant
```
layout(binding = 0) uniform UniformBufferObject {
    mat4 projection;
} ubo;
```

TODO
===
* make sure errors get output