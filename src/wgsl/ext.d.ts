declare module '*.glsl' {
    const shader: string;
    // @ts-ignore
    export default shader;
}

declare module '*.wgsl' {
    const shader: string;
    // @ts-ignore
    export default shader;
}

declare module '*.vert' {
    const shader: string;
    // @ts-ignore
    export default shader;
}

declare module '*.frag' {
    const shader: string;
    // @ts-ignore
    export default shader;
}
declare module '*.comp' {
    const shader: string;
    // @ts-ignore
    export default shader;
}
