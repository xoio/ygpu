import {createFilter} from "vite";
import {transformWithEsbuild} from "vite";
import path from "path"
import {process_file} from "./lib/glsl_wgsl"
import fs from "fs"

const resolve = path.resolve;
const dirname = path.dirname;
const FILE_TYPES = Object.freeze([
    "**/*.frag",
    "**/*.vert",
    "**/*.comp",
    "**/*.wgsl.vert",
    "**/*.wgsl.frag",
    "**/*.wgsl.comp",
])

enum SHADER_TYPE {
    VERTEX,
    FRAGMENT
}

export default async function (
    {
        shader_root = path.resolve(process.cwd(), "src", "shaders")
    } = {}
) {

    const filter = createFilter(FILE_TYPES)


    function resolveIncludes(path: string, current_dir: string, output:any[] = []) {

        let content = fs.readFileSync(path, "utf-8")
        let lines = content.split("\n")

        lines.forEach((line, i) => {
            if (line.search("#include") !== -1) {

                let path = line.replace("#include ", "")


                let ipath = resolve(current_dir, path)
                let parent = dirname(ipath)

                // not sure why there's an added return line but try to replace
                ipath = ipath.replace("\r", "")

                console.log(ipath)

                resolveIncludes(ipath, parent, output)

            } else {
                output.push(line + "\n")
            }
        })

        return output.join("\n")


    }

    return [
        {
            //enforce: "pre",
            name: "vite-plugin-juicer",

            async transform(src: any, id: any) {

                if (!filter(id)) return
                let output: any[] = []
                let content = resolveIncludes(id, shader_root, output)

                if (id.search("wgsl")) {
                    const pattern = /(vert|frag|comp)/g
                    const match = id.match(pattern)

                    if (match && match[0] === "vert") {
                        content = process_file(content, 0)
                    } else if (match && match[0] === "frag") {
                        content = process_file(content, 1)

                    }else if (match && match[0] === "comp"){
                        content = process_file(content,2)
                    }

                }

                return await transformWithEsbuild(content, id, {
                    loader: "text",
                    format: "esm"
                })

            }
        }
    ]
}
