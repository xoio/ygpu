import './style.css'

import samplelist from "./samples/samplelist";


(async function () {

    if (window.location.hash === "") {
        const li = samplelist.map(itm => {
            return `<li>
                   
                    <a href="#${itm.name}">${itm.name}</a>
                </li>`
        })
        let listing = document.querySelector("#samples")
        const app = document.querySelector("#app")

        // add title to the list
        let title = document.createElement("h1")
        title.innerHTML = "This is a list of examples using the library"
        app!.prepend(title)

        // build list
        li.forEach(itm => {
            listing!.innerHTML += itm
        })
    } else {
        const location = window.location.hash.replace("#", "")
        let sample = samplelist.filter(itm => itm.name === location)
        if (sample.length > 0) {
            let app = await import(sample[0].path)
            app.default()
        }
    }


    window.onhashchange = () => {
        window.location.reload()
    }

})()

