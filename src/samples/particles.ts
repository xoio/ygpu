import {createInstance} from "../ygpu/core/instance"
import {createShader} from "../ygpu/core/shader";
import {GPUInstance, ShaderModule} from "../ygpu/types";
import {
    CLEAR_OP,
    COPY_DST_USAGE,
    FLOAT32_2,
    FLOAT32_4,
    INDEX_USAGE,
    INSTANCE_STEP_MODE, LINEAR_FILTER,
    SHADER_FRAGMENT,
    SHADER_VERTEX,
    STORAGE_TYPE,
    STORAGE_USAGE,
    STORE_OP,
    UNIFORM_TYPE,
    UNIFORM_USAGE,
    VERTEX_USAGE
} from "../ygpu/constants";
import {mat4} from "../math/glmatrix/mat4";
import {createBindGroupEntries} from "../ygpu/bindgroup/bindgroup_entry";
import {createBindGroup} from "../ygpu/bindgroup/bindgroup";

import vertexShader from "./shaders/particles/particle.wgsl.vert"
import fragmentShader from "./shaders/particles/particle.wgsl.frag"
import computeShader from "./shaders/particles/particle.wgsl.comp"
import floorVert from "./shaders/floor/floor.wgsl.vert"
import floorFrag from "./shaders/floor/floor.wgsl.frag"
import fullscreenVert from "../shaders/fullscreen.wgsl.vert"
import fullscreenFrag from "./shaders/fullscreen/render.wgsl.frag"

import {createComputePipeline} from "../ygpu/core/compute";
import {createGraphicsPipeline} from "../ygpu/core/pipeline";
import {renderObject, RndrObject, runCompute} from "../ygpu/framework/object";
import {vec3} from "../math/glmatrix/vec3";

//@ts-ignore
import createSphere from "primitive-sphere"
import {randomPtInSphere} from "../math/utils";
import {PerspectiveCamera} from "../ygpu/framework/camera";
import OrbitCamera from "../ygpu/framework/orbitcamera";
import {getAlphaBlendingSettings} from "../ygpu/utils";
import {GradientLinear} from "../ygpu/framework/color";
import FrameBuffer from "../ygpu/framework/framebuffer";

const NUM_PARTICLES = 1000
const ZOOM = 10

// number of attributes for each particle in the compute stage
const NUM_PARTICLE_ATTRIBS = 3

type BackgroundCore = {
    pipeline: GPURenderPipeline,
    cameraBuffer: GPUBuffer,
    vertices: GPUBuffer
}

/**
 * A small sketch based on David Li's "Flow"
 * https://github.com/dli/flow/tree/master
 *
 * Includes a demonstration of how framebuffer's and compute shaders could work.
 *
 * Note that floating point values are assumed which is why we multiply things by 4. We also multiply
 * by 4 due to sticking with vec4 values which have a byte size of 4 to help make some of the math
 * a little easier to deal with.
 */
export default async function () {
    let canvas = document.querySelector("#canvas") as HTMLCanvasElement
    let instance = await createInstance({
        canvas
    }) as GPUInstance

    instance.addCanvasHelper(canvas)

    const compute = buildCompute(instance)
    const camera = getTransformationMatrix(instance)

    const bindgroup = buildBindGroup(instance, camera)
    const particles = buildMesh(instance, compute, [bindgroup.bindGroup], [bindgroup.bindgroup_layout])

    let backgroundCore = buildBGCore(instance, camera)
    let wall = buildWall(instance, backgroundCore)
    let floor = buildFloor(instance, backgroundCore)
    let framebuffer = new FrameBuffer(instance, window.innerWidth, window.innerHeight)
    let renderMesh = buildRenderOutput(instance, framebuffer.getAttachment())

    window.addEventListener("resize", () => {
        camera.updateCameraPerspective()
    })

    let lastTime = 0.0
    let render = (dt: number) => {
        requestAnimationFrame(render)
        let currentTime = dt;

        let deltaTime = (currentTime - lastTime) / 100 || 0.0;
        lastTime = currentTime;

        camera.update()
        const commandEncoder = instance.createCommandEncoder()
        const swapchain = instance.getView()

        const renderPassDescriptor: GPURenderPassDescriptor = {
            colorAttachments: [
                {
                    view: swapchain,
                    clearValue: [0, 0, 0, 1],
                    loadOp: CLEAR_OP,
                    storeOp: STORE_OP
                }
            ]
        }


        // run compute process
        {
            // update compute stage settings
            // update time and delta time
            compute.settings[0] = currentTime;
            compute.settings[1] = deltaTime


            // update settings buffer
            instance.writeBuffer(compute.settingsBuffer, 0, compute.settings, 0, 4);

            const passEncoder = commandEncoder.beginComputePass({})
            runCompute(compute.rndr as RndrObject, passEncoder)
            passEncoder.end()
        }

        // render content into framebuffer
        {
            const passEncoder = framebuffer.bind(commandEncoder)
            // render wall
            renderObject(wall, passEncoder)

            // render floor
            renderObject(floor, passEncoder)

            // render particles
            renderObject(particles, passEncoder)
            framebuffer.unbind()
        }

        // render the output
        {
            const passEncoder = commandEncoder.beginRenderPass(renderPassDescriptor)
            renderObject(renderMesh,passEncoder)
            passEncoder.end()
        }


        instance.submit([commandEncoder.finish()])
    }

    render(0.0)
}

/**
 * Builds the mesh and geometry to render
 * @param ctx
 * @param compute
 * @param bindgroups
 * @param bg_layouts
 */
function buildMesh(ctx: GPUInstance, compute: any, bindgroups: Array<GPUBindGroup>, bg_layouts: Array<GPUBindGroupLayout>) {
    const buffers = buildGeo(ctx)

    let colors = buildColors(ctx)

    const shader = createShader(
        ctx,
        vertexShader,
        fragmentShader,
        {
            buffers: [
                {

                    // vec4 - byte size of 4. multiply by byte size of float
                    arrayStride: 16,
                    attributes: [
                        {
                            shaderLocation: 0,
                            offset: 0,
                            format: FLOAT32_4
                        }
                    ]
                },
                {
                    // each attrib has byte size of 4 due to vec4. Multiply by
                    // 4 to get the byte size between each attribute.
                    arrayStride: 4 * (4 * NUM_PARTICLE_ATTRIBS),
                    stepMode: INSTANCE_STEP_MODE,
                    attributes: [
                        {
                            shaderLocation: 1,
                            offset: 0,
                            format: FLOAT32_4
                        }
                    ]
                },
                {
                    arrayStride: 4 * 4,
                    stepMode: INSTANCE_STEP_MODE,
                    attributes: [
                        {
                            shaderLocation: 2,
                            offset: 0,
                            format: FLOAT32_4
                        }
                    ]
                },
                {
                    arrayStride: 4 * 4,
                    stepMode: INSTANCE_STEP_MODE,
                    attributes: [
                        {
                            shaderLocation: 3,
                            offset: 0,
                            format: FLOAT32_4
                        }
                    ]
                }
            ],
            fragmentTargets: [
                {
                    format: ctx.getPreferredFormat(),
                    blend: getAlphaBlendingSettings()
                }
            ]
        })

    // mostly to appease typescript - you can just pass these directly too and ignore the line
    let v = shader?.vertex as ShaderModule
    let f = shader?.fragment as ShaderModule

    let pipeline = createGraphicsPipeline(ctx, v, f, {
        label: "geometry pipeline",

        layout: ctx.createPipelineLayout({
            bindGroupLayouts: bg_layouts
        }),

    })

    let obj: RndrObject = {
        pipeline,
        bindgroups,
        numInstances: NUM_PARTICLES,
        indexBuffer: buffers.indices,
        indexCount: buffers.indexCount,
        vertexBuffers: [buffers.vertices, compute.buffer, buffers.scaleBuffer, colors.buffer]

    }
    return obj
}

/**
 * Build render output
 */
function buildRenderOutput(ctx: GPUInstance, outputTexture: GPUTexture) {

    let verts: Array<number> = [
        1.0, 1.0, 1.0, 1.0,
        1.0, 1.0, 1.0, 1.0,
        1.0, 1.0, 1.0, 1.0
    ]
    // build buffers
    let vertData = new Float32Array(verts)
    const vertBuffer = ctx.createBuffer({
        size: vertData.byteLength,
        usage: VERTEX_USAGE,
        mappedAtCreation: true,
        label: "sphere vertices"
    }, vertData);

    let colors = buildColors(ctx)

    const shader = createShader(
        ctx,
        fullscreenVert,
        fullscreenFrag,
        {
            buffers: [
                {

                    // vec4 - byte size of 4. multiply by byte size of float
                    arrayStride: 16,
                    attributes: [
                        {
                            shaderLocation: 0,
                            offset: 0,
                            format: FLOAT32_4
                        }
                    ]
                }
            ]
        })

    // mostly to appease typescript - you can just pass these directly too and ignore the line
    let v = shader?.vertex as ShaderModule
    let f = shader?.fragment as ShaderModule

    let sampler = ctx.createSampler({
        magFilter: LINEAR_FILTER as GPUFilterMode,
        minFilter: LINEAR_FILTER as GPUFilterMode
    })


    let entries = createBindGroupEntries(
        [
            {
                location: 0,
                shader: GPUShaderStage.FRAGMENT,
                sampler:{}

            },
            {
                location: 1,
                shader: GPUShaderStage.FRAGMENT,
                texture:{}
            }
        ]
    )
    let bindgroup_layout = ctx.createBindGroupLayout({
        entries,
        label: "render output BG"
    }) as GPUBindGroupLayout

    let bg = createBindGroup(ctx, [
        {
            binding: 0,
            resource: sampler
        },
        {
            binding: 1,
            resource: outputTexture.createView()
        }
    ], {
        layout: bindgroup_layout,
        label: "compute bind group"
    })




    let pipeline = createGraphicsPipeline(ctx, v, f, {
        label: "render output pipeline",

        layout: ctx.createPipelineLayout({
            bindGroupLayouts: [bindgroup_layout]
        }),

    })

    let obj: RndrObject = {
        pipeline,
        bindgroups: [bg],
        vertexCount: 3,
        vertexBuffers: [vertBuffer]

    }
    return obj
}


/**
 * Build components for compute stage.
 * @param ctx {GPUInstance} the GPUInstance to use
 */
function buildCompute(ctx: GPUInstance) {
    const particles = buildParticles(ctx, NUM_PARTICLES);

    // build settings for the compute stage
    let settings = new Float32Array([

        // time
        // x = time
        // y = delta time
        0.0, 0.0, 0.0, 0.0,

        // general settings
        // x = time
        // Y = initial turbulence
        // z = base speed
        0.0, 0.2, 0.2, 0.0,

        // noise
        // x = noise octaves
        // y = noise position scale
        // z = noise scale
        // a = noise time scale
        3.0, 1.5, 0.075, 1 / 4000
    ])
    let settingsBuffer = ctx.createBuffer({
        size: settings.byteLength,
        usage: UNIFORM_USAGE | COPY_DST_USAGE,
        mappedAtCreation: true
    }, settings)

    let entries = createBindGroupEntries([
        {
            location: 0,
            shader: GPUShaderStage.COMPUTE,
            buffer: STORAGE_TYPE
        },
        {
            location: 1,
            shader: GPUShaderStage.COMPUTE,
            buffer: UNIFORM_TYPE
        }
    ])
    let debug_layout = ctx.createBindGroupLayout({
        entries
    })

    let bg = createBindGroup(ctx, [
        {
            binding: 0,
            resource: {
                buffer: particles
            }
        },
        {
            binding: 1,
            resource: {
                buffer: settingsBuffer
            }
        }
    ], {
        layout: debug_layout,
        label: "compute bind group"
    })

    let pipelineLayout = ctx.createPipelineLayout({
        bindGroupLayouts: [debug_layout]
    })

    let pipeline = createComputePipeline(ctx, {
        shader: computeShader,
        layout: pipelineLayout
    })

    return {

        buffer: particles,
        settings,
        settingsBuffer,
        rndr: {
            pipeline,
            bindgroups: [bg],
            compute: {
                xCount: 256,
                yCount: 1,
                zCount: 1
            }
        }
    }
}

/**
 * Builds the geometry we want to render
 * @param ctx
 */
function buildGeo(ctx: GPUInstance) {

    let sphere = createSphere(0.4, {
        segments: 16
    })

    let verts: Array<number> = []
    let cells: Array<number> = []

    // gonna make this a vec4 instead since it'll make the math and buffer layout a bit nicer.
    sphere.positions.forEach((itm: any) => {
        verts.push(itm[0], itm[1], itm[2], 1.0)
    })

    sphere.cells.forEach((itm: any) => {
        cells.push(itm[0], itm[1], itm[2])
    })

    // build buffers
    let vertData = new Float32Array(verts)
    const vertBuffer = ctx.createBuffer({
        size: vertData.byteLength,
        usage: VERTEX_USAGE,
        mappedAtCreation: true,
        label: "sphere vertices"
    }, vertData);

    let indexData = new Uint32Array(cells)

    let indexBuffer = ctx.createBuffer({
        size: indexData.byteLength,
        usage: INDEX_USAGE,
        mappedAtCreation: true,
        label: "sphere indices"
    }, indexData)


    // build instanced scale attribute
    let scale: number[] = []
    let scaleSize = 0.4
    for (let i = 0; i < NUM_PARTICLES * 4; i += 4) {
        const val = Math.random() * scaleSize
        scale.push(
            val,
            val,
            val,
            1.0
        )
    }

    let scaleData = new Float32Array(scale)

    let scaleBuffer = ctx.createBuffer({
        size: scaleData.byteLength,
        usage: VERTEX_USAGE,
        mappedAtCreation: true,
        label: "scale"
    }, scaleData)

    return {
        vertices: vertBuffer,
        indices: indexBuffer,
        indexCount: cells.length,
        scaleBuffer
    }
}

/**
 * Builds the main buffer for holding particle information
 * @param ctx
 * @param num
 */
function buildParticles(ctx: GPUInstance, num: number) {

    let data = new Float32Array(num * 4 * (NUM_PARTICLE_ATTRIBS * 4))


    for (let i = 0; i < data.length; i += (4 * NUM_PARTICLE_ATTRIBS)) {

        let spherePt = randomPtInSphere()

        // Position
        data[i] = spherePt.x
        data[i + 1] = spherePt.y
        data[i + 2] = spherePt.z
        data[i + 3] = 10.0 * Math.random() * 5 // life
        //data[i + 3] = 1.0

        // origin
        data[i + 4] = spherePt.x
        data[i + 5] = spherePt.y
        data[i + 6] = spherePt.z
        data[i + 7] = data[i + 3] // life

        // velocity
        data[i + 8] = 0.0
        data[i + 9] = 0.0
        data[i + 10] = 0.0
        data[i + 11] = 0.0

    }

    // build buffer for particles
    return ctx.createBuffer({
        size: data.byteLength,
        usage: STORAGE_USAGE | VERTEX_USAGE,
        mappedAtCreation: true,
        label: "compute vertices"
    }, data)
}

/**
 * Builds the main camera
 */
function getTransformationMatrix(ctx: GPUInstance) {

    const aspect = window.innerWidth / window.innerHeight

    let camera = new PerspectiveCamera(
        65.0,
        aspect,
        0.1,
        1000000.0
    );


    let orbitCam = new OrbitCamera()
    orbitCam.setDistance(ZOOM)

    // 1024 - 16x16
    let cam_data = new Float32Array(1024)
    cam_data.set(camera.projectionMatrix)
    cam_data.set(camera.viewMatrix, 16)

    let buffer = ctx.createBuffer({
        size: cam_data.byteLength,
        usage: UNIFORM_USAGE | COPY_DST_USAGE,
        mappedAtCreation: true,
        label: "camera"
    }, cam_data)

    return {
        orbitCam,
        cameraBuffer: buffer,
        cam_data,
        updateCameraPerspective() {
            camera.resize()
            camera.setViewMatrix(orbitCam.getViewMatrix())
        },

        update() {
            camera.setViewMatrix(orbitCam.getViewMatrix())
            const proj = camera.getProjectionMatrix()
            const view = camera.getViewMatrix()
            cam_data.set(proj)
            cam_data.set(view, 16)


            ctx.device.queue.writeBuffer(
                buffer,
                0,
                cam_data.buffer,
                cam_data.byteOffset,
                cam_data.byteLength
            );
        }
    }
}

/**
 * builds the main bindgroup for everything
 * @param ctx {GPUInstance}
 * @param camera
 */
function buildBindGroup(ctx: GPUInstance, camera: any) {
    const aspect = window.innerWidth / window.innerHeight

    // BUILD CAMERA
    let {cameraBuffer, cam_data} = camera


    let entries = createBindGroupEntries(
        [
            {
                location: 0,
                shader: GPUShaderStage.VERTEX,
                buffer: UNIFORM_TYPE
            },
        ]
    )
    let bindgroup_layout = ctx.createBindGroupLayout({
        entries,
        label: "Global BG"
    })
    let bg = createBindGroup(ctx, [
        {
            binding: 0,
            resource: {
                buffer: cameraBuffer
            }
        }
    ], {
        layout: bindgroup_layout,
        label: "Global bind group"
    })

    return {
        bindGroup: bg,
        bindgroup_layout,
        camera: {
            buffer: cameraBuffer,
            data: cam_data
        }
    }
}


function buildFloor(ctx: GPUInstance, bgCore: BackgroundCore) {

    //// build settings ///
    let settingsData = [
        // color
        1.0, 1.0, 0.0, 1.0,

        // meta settings
        // x = 1.0 says render floor, 0.0 says render wall
        1.0, 0.0, 0.0, 0.0
    ];

    const settings = new Float32Array(settingsData)

    let settingsBuffer = ctx.createBuffer({
        size: settings.byteLength,
        mappedAtCreation: true,
        usage: UNIFORM_USAGE | COPY_DST_USAGE
    }, settings)

    let entries = createBindGroupEntries(
        [
            {
                location: 0,
                shader: SHADER_FRAGMENT | SHADER_VERTEX,
                buffer: UNIFORM_TYPE
            },
            {
                location: 1,
                shader: SHADER_VERTEX,
                buffer: UNIFORM_TYPE
            }
        ]
    )

    let bindgroup_layout = ctx.createBindGroupLayout({
        entries,
        label: "floor bind group"
    })

    let bg = createBindGroup(ctx, [
        {
            binding: 0,
            resource: {
                buffer: settingsBuffer
            }
        },
        {
            binding: 1,
            resource: {
                buffer: bgCore.cameraBuffer
            }
        }
    ], {
        layout: bindgroup_layout,
        label: "floor bind group"
    })

    return {
        pipeline: bgCore.pipeline,
        bindgroups: [bg],
        vertexBuffers: [bgCore.vertices],
        vertexCount: 5,
        settingsBuffer,
        settingsData
    }
}

/**
 * Builds the background wall
 * @param ctx
 * @param bgCore
 */
function buildWall(ctx: GPUInstance, bgCore: BackgroundCore) {

    //// build settings ///
    let settingsData = [
        // color
        1.0, 0.0, 0.0, 1.0,

        // meta settings
        // x = 1.0 says render floor, 0.0 says render wall
        0.0, 0.0, 0.0, 0.0
    ];

    const settings = new Float32Array(settingsData)

    let settingsBuffer = ctx.createBuffer({
        size: settings.byteLength,
        mappedAtCreation: true,
        usage: UNIFORM_USAGE | COPY_DST_USAGE
    }, settings)

    let entries = createBindGroupEntries(
        [
            {
                location: 0,
                shader: SHADER_FRAGMENT | SHADER_VERTEX,
                buffer: UNIFORM_TYPE
            },
            {
                location: 1,
                shader: SHADER_VERTEX,
                buffer: UNIFORM_TYPE
            }
        ]
    )

    let bindgroup_layout = ctx.createBindGroupLayout({
        entries,
        label: "wall bind group"
    })

    let bg = createBindGroup(ctx, [
        {
            binding: 0,
            resource: {
                buffer: settingsBuffer
            }
        },
        {
            binding: 1,
            resource: {
                buffer: bgCore.cameraBuffer
            }
        }
    ], {
        layout: bindgroup_layout,
        label: "floor bind group"
    })

    return {
        pipeline: bgCore.pipeline,
        bindgroups: [bg],
        vertexBuffers: [bgCore.vertices],
        vertexCount: 5,
        settingsBuffer,
        settingsData
    }
}

/**
 * Builds the core components for the background elements
 *
 * @param ctx {GPUInstance}
 */
function buildBGCore(ctx: GPUInstance, camera: any): BackgroundCore {

    // build main camera
    let projectionMatrix = mat4.create()
    let viewMatrix = mat4.create()

    let aspect = window.innerWidth / window.innerHeight
    mat4.translate(viewMatrix, viewMatrix, vec3.fromValues(0, 0, -1))
    mat4.perspectiveZO(projectionMatrix, Math.PI / 2, aspect, 0.1, 100000.0);

    // 1024 - 16x16
    let cam_data = new Float32Array(1024)
    cam_data.set(projectionMatrix)
    cam_data.set(viewMatrix, 16)

    let cam_buffer = ctx.createBuffer({
        size: cam_data.byteLength,
        usage: UNIFORM_USAGE,
        mappedAtCreation: true,
        label: "camera"
    }, cam_data)


    const shader = createShader(
        ctx,
        floorVert,
        floorFrag, {
            buffers: [
                {
                    arrayStride: 8,
                    attributes: [
                        {
                            shaderLocation: 0,
                            offset: 0,
                            format: FLOAT32_2
                        }
                    ]
                },
            ]
        }
    )

    let entries = createBindGroupEntries(
        [
            {
                location: 0,
                shader: SHADER_FRAGMENT | SHADER_VERTEX,
                buffer: UNIFORM_TYPE
            },
            {
                location: 1,
                shader: SHADER_VERTEX,
                buffer: UNIFORM_TYPE
            }
        ]
    )

    let bindgroup_layout = ctx.createBindGroupLayout({
        entries,
        label: "background bind group"
    })
    let pipelineLayout = ctx.createPipelineLayout({
        bindGroupLayouts: [bindgroup_layout],
    })

    // build vertices
    let v = new Float32Array([
        1.0, 1.0,
        -1.0, 1.0,
        -1.0, -1.0,
        1.0, -1.0,
        1.0, 1.0,
    ])

    let vertBuffer = ctx.createBuffer({
        size: v.byteLength,
        mappedAtCreation: true,
        usage: VERTEX_USAGE,
        label: "vertex"
    }, v)
    return {
        pipeline: createGraphicsPipeline(
            ctx,
            shader?.vertex as ShaderModule,
            shader?.fragment as ShaderModule,
            {
                layout: pipelineLayout,
                topology: "triangle-strip",
                label: "floor",
                cullMode: "none"
            }
        ),
        //cameraBuffer: cam_buffer,
        cameraBuffer: camera.cameraBuffer,
        vertices: vertBuffer
    }
}

function buildColors(ctx: GPUInstance) {
    const palette = [
        "#EF2006", "#350000", "#A11104", "#ED5910", "#F1B52E", "#7B5614", "#F7F1AC",
    ]

    let gradient = new GradientLinear(palette)
    let colorData = new Float32Array(NUM_PARTICLES * 4)

    let idx = 0
    for (let i = 0; i < NUM_PARTICLES; ++i) {
        let set = gradient.get_at(Math.random())
        colorData.set(set, idx)
        idx += 4
    }

    let colorBuffer = ctx.createBuffer({
        size: colorData.byteLength,
        usage: VERTEX_USAGE,
        mappedAtCreation: true
    }, colorData)

    return {
        buffer: colorBuffer,
        colorData
    }
}

/*
function buildShadowComponents(ctx: GPUInstance) {

    let lightViewMatrix = mat4.create()
    mat4.lookAt(lightViewMatrix, [0.0, 0.0, 0.0], [0.0, -1.0, 0.0], [0.0, 0.0, 1.0])
}
 */
