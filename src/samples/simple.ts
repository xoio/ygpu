import {createInstance} from "../ygpu/core/instance"
import {createShader} from "../ygpu/core/shader";
import {GPUInstance, ShaderModule} from "../ygpu/types";
import vert from "./shaders/simple/simple.wgsl.vert"
import frag from "./shaders/simple/simple.wgsl.frag"
import {CLEAR_OP, FLOAT32_4, STORE_OP, VERTEX_USAGE} from "../ygpu/constants";
import {createGraphicsPipeline} from "../ygpu/core/pipeline";

const OFFSCREEN = true

/**
 * A basic hello triangle sample. Adapted from official WebGPU samples. Also demonstrates Offscreen canvas support.
 * https://webgpu.github.io/webgpu-samples/?sample=helloTriangle
 */
export default async function () {
    let renderCanvas = null
    let canvas = null
    let renderContext = null

    if (OFFSCREEN) {
        canvas = new OffscreenCanvas(window.innerWidth, window.innerHeight) as OffscreenCanvas
        renderCanvas = document.querySelector("#canvas") as HTMLCanvasElement
        renderContext = renderCanvas.getContext("bitmaprenderer")

    } else {
        canvas = document.querySelector("#canvas") as HTMLCanvasElement
    }

    let instance = await createInstance({
        canvas
    }) as GPUInstance

    // allow auto resizing of canvas.
    if (!OFFSCREEN) {
        instance.addCanvasHelper(canvas as HTMLCanvasElement)
    } else {
        instance.addCanvasHelper(renderCanvas as HTMLCanvasElement)
    }
    // build vertex buffer
    const verts = new Float32Array([
        0.0, 0.5, 0.0, 1.0,
        -0.5, -0.5, 0.0, 1.0,
        0.5, -0.5, 0.0, 1.0
    ])

    const vertBuffer = instance.createBuffer({
        size: verts.byteLength,
        usage: VERTEX_USAGE,
        mappedAtCreation: true,
        label: "vertices"
    }, verts);

    // build shader
    let shader = createShader(instance, vert, frag, {
        buffers: [
            {
                arrayStride: 16, // 4 * 4 , 4 because vec4. Multiply by 4 because a float is 4 bytes
                attributes: [
                    {
                        shaderLocation: 0,
                        offset: 0,
                        format: FLOAT32_4
                    }
                ]
            }
        ],
        fragmentTargets: [
            {
                format: instance.getPreferredFormat()
            }
        ]
    })

    // mostly to appease typescript - you can just pass these directly too and ignore the line
    let v = shader?.vertex as ShaderModule
    let f = shader?.fragment as ShaderModule

    // build pipeline
    let pipeline = createGraphicsPipeline(instance, v, f)

    // render
    let render = () => {
        requestAnimationFrame(render)

        const commandEncoder = instance.createCommandEncoder()
        const swapchain = instance.getView()

        const renderPassDescriptor: GPURenderPassDescriptor = {
            colorAttachments: [
                {
                    view: swapchain,
                    clearValue: [0, 0, 0, 1],
                    loadOp: CLEAR_OP,
                    storeOp: STORE_OP
                }
            ]
        }

        const passEncoder = commandEncoder.beginRenderPass(renderPassDescriptor)
        passEncoder.setPipeline(pipeline)
        passEncoder.setVertexBuffer(0, vertBuffer)
        passEncoder.draw(3)
        passEncoder.end()

        instance.submit([commandEncoder.finish()])

        if (OFFSCREEN) {
            const c = canvas as OffscreenCanvas
            const r = renderCanvas as HTMLCanvasElement
            const bitmap = c.transferToImageBitmap()
            renderContext!.transferFromImageBitmap(bitmap)

        }
    }

    render()

}

