#version 450

layout(binding = 0) uniform UniformBufferObject {
    mat4 proj;
    mat4 view;
} ubo;

layout(location=0) in vec4 position;
layout(location=1) in vec4 iPosition;
layout(location=2) in vec4 iScale;
layout(location=3) in vec4 iColor;


layout(location=0) out vec4 vCol;
void main(){
    vec3 pos = (position.xyz * iScale.xyz) + iPosition.xyz;
    gl_Position = ubo.proj * ubo.view * vec4(pos,1.);

    vCol = normalize(iColor);
    vCol.a = iPosition.a;
}
