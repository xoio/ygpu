#version 450
layout(set=0, binding=0) uniform sampler uSampler;
layout(set=0, binding=1) uniform texture2D uTex;

layout(location = 0)in vec2 vUv;
layout(location=0)out vec4 glFragColor;

void main(){
    vec4 img = texture(sampler2D(uTex, uSampler), vUv);
    glFragColor = img;
}

