#version 440

layout(binding = 1) uniform UniformBufferObject {
    mat4 proj;
    mat4 view;
} ubo;

layout(binding = 0) uniform Settings {
    vec4 color;
    vec4 meta;
} env;

layout(location = 0)in vec2 position;
layout(location = 0)out vec3 vPos;

void main(){

    if (env.meta.x == 1.0){
        vPos = vec3(position.x*200.0, -10.0, position.y * 200.);
        gl_Position =  ubo.proj * ubo.view * vec4(vPos, 1.);
    } else {
        vPos = vec3(position.x*20.0, position.y*200.0, 0.0);
        gl_Position =  vec4(vPos, 1.);
    }
}
