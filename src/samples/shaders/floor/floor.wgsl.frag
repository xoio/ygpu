#version 450

layout(binding = 0) uniform Settings {
    vec4 color;
    vec4 meta;
} env;
layout(location=0) in vec3 vPos;
layout(location=0) out vec4 glFragColor;
void main(){
    float val = 0.7;
    vec4 col = vec4(1.);

    if (env.meta.x == 1.0){
        col = vec4(val,val,val, smoothstep(84.0, 0.0, length(vPos)));
    } else {
        col = vec4(val,val,val, smoothstep(4.0, 0.0, length(vPos)));
    }


    glFragColor = vec4(env.color);
    glFragColor = col;
}
